#!/usr/bin/env python
"""Compiles the webcam photos into a timelapse movie.

Usage:
make_movie.py top ['<start_date>' '<end_date>']

Examples:
# Get help
make_movie.py --help

# Make a movie from images taken between June 1 and July 31 (inclusive)
make_movie.py ~/Documents/wx/cam '2010-06-01' '2010-07-31'

# Specify the build directory
make_movie.py ~/Documents/wx/cam --build_dir=/tmp

# Get help:
make_movie.py -h

# Run the tests with:
python -m doctest make_movie.py
# or
python -m doctest make_movie.py -v
"""


import os
import shutil
import argparse
import datetime
import time
import tempfile
import pyexiv2
import simplejson
import copy
#from PIL import Image
from pygooglechart import SparkLineChart
from pygooglechart import Axis
from urllib2 import URLError
from utils import run


# Or use 'Exif.Image.ImageDescription' to view the description in Nautilus
EXIF_FIELD = 'Exif.Image.ImageDescription' #'Exif.Photo.UserComment'
DATE_FMT = '%Y%m%d%H%M%S'


class Classtionary(dict):
    """A Class, er, dictionary, um, attributes-as-keys thingy.

    >>> c = Classtionary(foo="bar")
    >>> d = dict(foo="bar")
    >>> c, d
    ({'foo': 'bar'}, {'foo': 'bar'})
    >>> c.fizz = "bang"
    >>> d.fizz = "bang"
    Traceback (most recent call last):
      File "<input>", line 1, in <module>
    AttributeError: 'dict' object has no attribute 'fizz'
    >>> d['fizz'] = "bang"
    >>> c['fizz']
    'bang'
    >>> c.fizz
    'bang'
    >>> d['fizz']
    'bang'
    >>> d.fizz
    Traceback (most recent call last):
      File "<input>", line 1, in <module>
    AttributeError: 'dict' object has no attribute 'fizz'
    """

    def __init__(self, **kwargs):
        self.__dict__ = self
        self.update(kwargs)


def dirpath_to_datetime(pth):
    """Converts dirpath to a datetime object
    Assumes the dirpath includes
    /optional/prefix/year/month/day/hhmm.jp[e]g
    """
    fn = os.path.splitext(os.path.basename(pth))[0]
    hour = int(fn[:2])
    minute = int(fn[2:])

    parts = os.path.dirname(pth).split('/')
    parts.reverse()
    day = int(parts[0])
    month = int(parts[1])
    year = int(parts[2])
    dt = datetime.datetime(year, month, day, hour, minute)
    return dt


def filter_between_dates(mm_obj):
    """Keeps filenames that are between the start and end dates."""
    ret_val = []
    for idx, fn in enumerate(mm_obj.filenames):
        try:
            this_date = dirpath_to_datetime(os.path.join(mm_obj.dirpath, fn))
        except ValueError:
            # This file doesn't match the expected naming format.  It may be a
            # temporary file that wasn't completely written.
            continue
        if this_date >= mm_obj.start_date and this_date <= mm_obj.end_date:
            ret_val.append(fn)
    return ret_val


def get_image_data(image_file_path):
    """Returns a dictionary of JSON data stored in the image's EXIF_FIELD field.
    If the data doesn't exist, an empty dictionary is returned.

    See also: set_image_wx_data()

    >>> import os, shutil
    >>> os.chdir(os.path.dirname(os.path.abspath(__file__)))
    >>> test_img = './tests/deleteme.jpeg'
    >>> shutil.copy2('./tests/test_image.jpeg', test_img)
    >>> set_image_data(test_img, {u'test_data': u'Hello get_image_data!'})
    >>> get_image_data(test_img)
    {'test_data': 'Hello get_image_data!'}
    >>> os.remove(test_img)
    """
    metadata = pyexiv2.ImageMetadata(image_file_path)
    metadata.read()
    return simplejson.loads(metadata[EXIF_FIELD].raw_value)


def set_image_data(image_file_path, data):
    """Sets the EXIF_FIELD field of the image to a JSON version
    of the data.

    >>> import os, shutil
    >>> import datetime as dt
    >>> test_img = './tests/deleteme.jpeg'
    >>> shutil.copy2('./tests/test_image.jpeg', test_img)
    >>> set_image_data(test_img, {u'test_data': u'Hello set_image_data!'})
    >>> get_image_data(test_img)
    {'test_data': 'Hello set_image_data!'}
    >>> set_image_data(test_img, {'test_date': dt.datetime(2010, 5, 7, 13, 14)})
    >>> get_image_data(test_img)
    {'test_date': '20100507131400'}
    >>> os.remove(test_img)
    """
    metadata = pyexiv2.ImageMetadata(image_file_path)
    metadata.read()
    for k, v in data.items():
        if isinstance(v, datetime.datetime):
            data[k] = v.strftime(DATE_FMT)
    metadata[EXIF_FIELD] = simplejson.dumps(data)
    metadata.write()


class WxData(object):
    """A class to encapsulate methods used to parse the weather data.
    
    >>> import datetime
    >>> date_fmt = '%Y%m%d%H%M%S'
    >>> dt_day = datetime.datetime(2010, 07, 18, 11, 10)
    >>> dt_next_row = datetime.datetime(2010, 07, 18, 11, 15)
    >>> dt_night = datetime.datetime(2010, 07, 18, 01, 45)
    >>> dt_missing_row = datetime.datetime(2010, 7, 17, 13, 0)
    >>> dt_past_end = datetime.datetime(2110, 1, 1, 0, 0)
    >>> test_wx = open('./tests/test_wx_data.log', 'r')
    >>> w = WxData(dt_day, test_wx)
    >>> wx_data = w.get_wx_data()
    >>> keys = wx_data.keys()
    >>> keys.sort()
    >>> print(keys)
    ['Date', 'Dewpoint', 'Forecast', 'Rain_1h', 'Rain_24h', 'Rain_total', 'Rel_humidity_indoor', 'Rel_humidity_outdoor', 'Rel_pressure', 'Temp_indoor', 'Temp_outdoor', 'Tendency', 'Time', 'Timestamp', 'Wind_chill', 'Wind_direction-degrees', 'Wind_direction_text', 'Wind_speed']
    
    """
    
    def __init__(self, timestamp, wx_file_obj, seconds_resolution=None):
        self.timestamp = timestamp
        # we take a weather reading every 5 minutes, 300 seconds.
        # We want our found row to be within 1/2 of that, or 150 seconds.
        self.seconds_resolution = seconds_resolution or 150
        self.wx_file_obj = wx_file_obj
        self.field_names = (
            'Timestamp', 'Date', 'Time', 'Temp_indoor', 'Temp_outdoor', 
            'Dewpoint', 'Rel_humidity_indoor', 'Rel_humidity_outdoor', 
            'Wind_speed', 'Wind_direction-degrees', 'Wind_direction_text',
            'Wind_chill', 'Rain_1h', 'Rain_24h', 'Rain_total', 'Rel_pressure',
            'Tendency', 'Forecast',
        )
        self.data = {}
        self.pth = os.path.abspath(wx_file_obj.name)
        self.file_size = os.path.getsize(self.pth)

    def parse_line(self, line):
        """Parse a line and return a dictionary"""
        self.data = line.split()
        if len(self.data) < len(self.field_names):
            # This is a nighttime line, insert a some null data in the missing
            # fields
            # Tendency and Forecast
            self.data.extend([None, None])
            # Rain_1h and Rain_24h
            self.data.insert(10, None)
            self.data.insert(11, None)
        ret_val = Classtionary(**dict(zip(self.field_names, self.data)))
        # Make the first item a datetime object
        try:
            ret_val.Timestamp = datetime.datetime.strptime(
                ret_val.Timestamp, '%Y%m%d%H%M%S')
        except TypeError, e:
            # Trouble with this weather data, return whatever data we have
            print(
                "Error parsing the Timestamp from this wx file line:\n'%s'"
                % line)
        return ret_val

    def is_match(self, wx_data_line):
        """Returns True if wx_data_line is within seconds_resolution of
        timestamp
        """
        try:
            seconds_diff = abs(self.timestamp-wx_data_line.Timestamp).seconds
            if bool(seconds_diff <= self.seconds_resolution):
                return True
            else:
                return False
        except TypeError:
            print(
                "TypeError when calculating the time distance to our target "
                "timestamp.  timestamp: '%s'; wx_data.Timestamp: '%s'"
                % (self.timestamp, wx_data.Timestamp))
            return False

    def search(self, start_pos, end_pos, idx=None):
        """Return True if we've found the data line we are looking for."""
        search_pos = start_pos + ((end_pos - start_pos) // 2)
        self.wx_file_obj.seek(search_pos)
        if idx is None:
            idx = 0
        # This will likely return a partial line...
        line = self.wx_file_obj.readline()
        # ...so we'll look at the following line
        line = self.wx_file_obj.readline()
        d = self.parse_line(line)
        if not d.get('Timestamp', False): 
            return d
        if self.is_match(d):
            # Found it, return the data line
            return d
        else:
            # Is this as close as we can get?
            if idx and idx > 50:
                # We've recursed 50 levels, probably as close as we can get
                # so just return this row
                return d

            # Keep looking
            cur_pos = self.wx_file_obj.tell() - (len(line) + 1)
            if self.timestamp < d['Timestamp']:
                # search earlier in the file
                return self.search(start_pos, cur_pos, idx+1)
            else:
                # search later in the file
                return self.search(cur_pos, end_pos, idx+1)

    def get_wx_data(self):
        start_pos = self.wx_file_obj.tell()
        if start_pos == 0:
            # We are at the beginning of the file
            # Search for the datestamp with a binary search
            end_pos = self.file_size
            data = self.search(start_pos, end_pos)
        else:
            # Assume we've already found a line and are looking for the next line
            while True:
                line = self.wx_file_obj.readline()
                if not line:
                    # We've reached the end of the file, try a binary search
                    # instead
                    end_pos = file_size
                    start_pos = 0
                    data = self.search(start_pos, end_pos)
                    break
                else:
                    data = self.parse_line(line)
                    if self.is_match(data):
                        # Found it
                        break
                    elif data.Timestamp > self.timestamp:
                        # We're past the line we are looking for so fall back to a
                        # binary search
                        end_pos = self.wx_file_obj.tell()
                        start_pos = 0
                        data = self.search(start_pos, end_pos)
                        break
        return data

def get_wx_data(timestamp, wx_file_obj, seconds_resolution=None):
    """Returns a dictionary of weather data from the file object as close in
    time as possible to the timestamp.  We will search for weather data within
    seconds_resolution of timestamp.

    This function uses a binary search if its the first pass through the file
    or if the current record is past the timestamp we are looking for.
    Otherwise the search proceeds one row at a time through the weather data
    file.

    Assumes the weather file is sorted by Timestamp in ascending order and that
    the weather file format is similar to this (from a LaCrosse weather
    station):
    Timestamp Date Time Temp_indoor Temp_outdoor Dewpoint Rel_humidity_indoor Rel_humidity_outdoor Wind_speed Wind_direction-degrees Wind_direction_text Wind_chill Rain_1h Rain_24h Rain_total Rel_pressure Tendency Forecast
    20100718111016 2010-Jul-18 11:10:16 91.6 84.9 56.4 35 38 1.1 247.5 WSW 84.9 0.00 0.00 7.97 25.189 Rising Sunny
    or
    Timestamp Date Time Temp_indoor Temp_outdoor Dewpoint Rel_humidity_indoor Rel_humidity_outdoor Wind_speed Wind_direction-degrees Wind_direction_text Wind_chill Rain_1h Rain_24h Rain_total Rel_pressure
    20100718014500 2010-Jul-18 01:45:00 67.6 64.2 51.4 49 63 0.0 112.5 ESE 64.2 7.97 25.207

    >>> import datetime
    >>> date_fmt = '%Y%m%d%H%M%S'
    >>> dt_day = datetime.datetime(2010, 07, 18, 11, 10)
    >>> dt_next_row = datetime.datetime(2010, 07, 18, 11, 15)
    >>> dt_night = datetime.datetime(2010, 07, 18, 01, 45)
    >>> dt_missing_row = datetime.datetime(2010, 7, 17, 13, 0)
    >>> dt_past_end = datetime.datetime(2110, 1, 1, 0, 0)
    >>> test_wx = open('./tests/test_wx_data.log', 'r')

    >>> wx_data = get_wx_data(dt_day, test_wx)
    >>> keys = wx_data.keys()
    >>> keys.sort()
    >>> print(keys)
    ['Date', 'Dewpoint', 'Forecast', 'Rain_1h', 'Rain_24h', 'Rain_total', 'Rel_humidity_indoor', 'Rel_humidity_outdoor', 'Rel_pressure', 'Temp_indoor', 'Temp_outdoor', 'Tendency', 'Time', 'Timestamp', 'Wind_chill', 'Wind_direction-degrees', 'Wind_direction_text', 'Wind_speed']
    >>> get_wx_data(dt_day, test_wx)['Timestamp']
    datetime.datetime(2010, 7, 18, 11, 10, 16)
    >>> get_wx_data(dt_next_row, test_wx)['Timestamp']
    datetime.datetime(2010, 7, 18, 11, 15, 14)
    >>> get_wx_data(dt_night, test_wx).Timestamp
    datetime.datetime(2010, 7, 18, 1, 45)
    >>> get_wx_data(dt_missing_row, test_wx).Timestamp
    datetime.datetime(2010, 7, 17, 13, 10, 25)
    >>> get_wx_data(dt_day, test_wx).Rel_pressure
    '25.189'
    >>> get_wx_data(dt_day, test_wx).Tendency
    'Rising'
    >>> get_wx_data(dt_day, test_wx).Forecast
    'Sunny'
    >>> get_wx_data(dt_past_end, test_wx).Timestamp
    Error parsing the Timestamp from this wx file line:
    ''
    """
    if seconds_resolution is None:
        # we take a weather reading every 5 minutes, 300 seconds.  We want our
        # found row to be within 1/2 of that, or 150 seconds.
        seconds_resolution = 150

    field_names = (
        'Timestamp', 'Date', 'Time', 'Temp_indoor', 'Temp_outdoor', 'Dewpoint',
        'Rel_humidity_indoor', 'Rel_humidity_outdoor', 'Wind_speed',
        'Wind_direction-degrees', 'Wind_direction_text', 'Wind_chill',
        'Rain_1h', 'Rain_24h', 'Rain_total', 'Rel_pressure', 'Tendency', 
        'Forecast',
    )

    data = {}

    def parse_line(line):
        """Parse a line and return a dictionary"""
        data = line.split()
        if len(data) < len(field_names):
            # This is a nighttime line, insert a some null data in the missing
            # fields
            # Tendency and Forecast
            data.extend([None, None])
            # Rain_1h and Rain_24h
            data.insert(10, None)
            data.insert(11, None)
        ret_val = Classtionary(**dict(zip(field_names, data)))
        if not ret_val.Timestamp:
            ret_val.Timestamp = datetime.datetime.now()
        # Make the first item a datetime object
        try:
            ret_val.Timestamp = datetime.datetime.strptime(
                ret_val.Timestamp, '%Y%m%d%H%M%S')
        except TypeError, e:
            # Trouble with this weather data, return whatever data we have
            print(
                "Error parsing the Timestamp from this wx file line:\n'%s'"
                % line)
        return ret_val

    def is_match(wx_data_line):
        """Returns True if wx_data_line is within seconds_resolution of
        timestamp
        """
        try:
            seconds_diff = abs(timestamp-wx_data_line.Timestamp).seconds
            if bool(seconds_diff <= seconds_resolution):
                return True
            else:
                return False
        except TypeError:
            print(
                "TypeError when calculating the time distance to our target "
                "timestamp.  timestamp: '%s'; wx_data.Timestamp: '%s'"
                % (timestamp, wx_data.Timestamp))
            return False

    def search(start_pos, end_pos, idx=None):
        """Return True if we've found the data line we are looking for."""
        search_pos = start_pos + ((end_pos - start_pos) // 2)
        wx_file_obj.seek(search_pos)
        if idx is None:
            idx = 0
        # This will likely return a partial line...
        line = wx_file_obj.readline()
        # ...so we'll look at the following line
        line = wx_file_obj.readline()
        d = parse_line(line)
        if not d.get('Timestamp', False): 
            return d
        if is_match(d):
            # Found it, return the data line
            return d
        else:
            # Is this as close as we can get?
            if idx and idx > 50:
                # We've recursed 50 levels, probably as close as we can get
                # so just return this row
                return d

            # Keep looking
            cur_pos = wx_file_obj.tell() - (len(line) + 1)
            if timestamp < d['Timestamp']:
                # search earlier in the file
                return search(start_pos, cur_pos, idx+1)
            else:
                # search later in the file
                return search(cur_pos, end_pos, idx+1)

    start_pos = wx_file_obj.tell()
    pth = os.path.abspath(wx_file_obj.name)
    file_size = os.path.getsize(pth)

    if start_pos == 0:
        # We are at the beginning of the file
        # Search for the datestamp with a binary search
        end_pos = file_size
        data = search(start_pos, end_pos)
    else:
        # Assume we've already found a line and are looking for the next line
        while True:
            line = wx_file_obj.readline()
            if not line:
                # We've reached the end of the file, try a binary search
                # instead
                end_pos = file_size
                start_pos = 0
                data = search(start_pos, end_pos)
                break
            else:
                data = parse_line(line)
                if is_match(data):
                    # Found it
                    break
                elif data['Timestamp'] > timestamp:
                    # We're past the line we are looking for so fall back to a
                    # binary search
                    end_pos = wx_file_obj.tell()
                    start_pos = 0
                    data = search(start_pos, end_pos)
                    break
    return data


def get_wx_chart(timestamp, wxdatawindow):
    """Returns a chart of the current weather data window."""
    WIDTH = 960
    HEIGHT = 110
    LTPINK = 'ffb0d0f0'
    PINK = 'ff0066f0'
    RED = 'ff0000ff'
    LTBLUE = 'b4b2fff0'
    BLUE = '0600fff0'
    LBLCOLOR = '202020'

    chart = SparkLineChart(WIDTH, HEIGHT)
    current_temp = [d[wxdatawindow.IDX_TMP] for d in wxdatawindow.data]
    dewpoint = [d[wxdatawindow.IDX_DEWPT] for d in wxdatawindow.data]
    chart.add_data(current_temp)
    chart.add_data(dewpoint)
    chart.set_colours([RED, BLUE, ])
    chart.set_line_style(index=0, thickness=4)
    chart.set_line_style(index=1, thickness=4)

    index = chart.set_axis_labels(
        Axis.BOTTOM, [
            'Temp: %sF    Dewpoint: %sF'
            % (
                wxdatawindow.data[0][wxdatawindow.IDX_TMP],
                wxdatawindow.data[0][wxdatawindow.IDX_DEWPT],)
            ]
        )
    chart.set_axis_style(index, LBLCOLOR, font_size=16, alignment=-1)
    chart.set_axis_positions(index, [0])

    index = chart.set_axis_labels(
        Axis.BOTTOM, [timestamp.strftime('%m/%d/%Y %H:%M')])
    chart.set_axis_style(index, LBLCOLOR, font_size=14, alignment=-1)
    chart.set_axis_positions(index, [0])
    return chart


class WxDataWindow(object):
    """Represents a sliding window of weather data, samples, each including:
        1. Record high temperature
        2. Daily high temperature
        3. Current temperature
        4. Daily low temperature
        5. Record low temperature
        6. Dew Point

    >>> wdw = WxDataWindow(samples=3)
    >>> wdw.samples
    3

    """
    IDX_RHI = 0
    IDX_DHI = 1
    IDX_TMP = 2
    IDX_DLO = 3
    IDX_RLO = 4
    IDX_DEWPT = 5

    def __init__(self, samples=None):
        """Initialize the object with the number of samples to maintain, which
        defines how wide the sliding window is.
        samples will default to 96 (12 samples per hour x 8 hours) if not
        defined by the user.


        >>> wdw = WxDataWindow()
        >>> print(len(wdw._data))
        96
        >>> print(len(wdw._data[0]))
        6
        >>> print(wdw._data[0])
        [None, None, None, None, None, None]
        >>> wdw = WxDataWindow(samples=2)
        >>> print(wdw._data)
        [[None, None, None, None, None, None], [None, None, None, None, None, None]]
        """
        self._data = []
        for i in range((samples or 96)):
            # 96 = 12 samples/hr * 8hrs)
            self._data.append([None, None, None, None, None, None])

    @property
    def samples(self):
        """Returns the number of data samples we are storing.
        >>> wdw = WxDataWindow()
        >>> print(wdw.samples)
        96
        >>> wdw = WxDataWindow(samples=2)
        >>> print(wdw.samples)
        2
        """
        return len(self._data)

    @property
    def data(self):
        """Returns a two dimensional list, the samples, of data this object
        is maintaining.

        >>> wdw = WxDataWindow(samples=2)
        >>> print(wdw.data)
        [[None, None, None, None, None, None], [None, None, None, None, None, None]]
        """
        return self._data

    def put(self, temperature, dewpt):
        """Add the temperature and dewpoint to our sliding window of data while
        updating the historical highs and lows in the new sample.  Pops the
        oldest data from the list and inserts the newest at the front of the
        list.  In this code, a 'sample' is a list of 5 data points, including:
            1. Record high temperature
            2. Daily high temperature
            3. Current temperature
            4. Daily low temperature
            5. Record low temperature
            6. Dewpoint
        Note that 'daily' values may span more than one day if the size of the
        sliding window (number of samples) is greater than one day.  Daily
        really means 'over the number of samples in our window'.

        >>> wdw = WxDataWindow(samples=2)
        >>> wdw.put(temperature=55, dewpt=45)
        >>> print(wdw.data)
        [[55, 55, 55, 55, 55, 45], [None, None, None, None, None, None]]
        >>> wdw.put(temperature=76, dewpt=50)
        >>> print(wdw.data)
        [[76, 76, 76, 55, 55, 50], [55, 55, 55, 55, 55, 45]]
        """
        # Make a copy of the most recent data, which we'll update
        newest = copy.copy(self._data[0])
        # Set the current temperature
        newest[self.IDX_TMP] = temperature
        # Set the current dewpoint
        newest[self.IDX_DEWPT] = dewpt
        # Throw away the oldest data
        self._data.pop()

        # Check for record high and record low
        if newest[self.IDX_RHI] is None or temperature > newest[self.IDX_RHI]:
            newest[self.IDX_RHI] = temperature
        if newest[self.IDX_RLO] is None or temperature < newest[self.IDX_RLO]:
            newest[self.IDX_RLO] = temperature

        # Check for daily high and low - we have to check each of the samples
        dailyhi = -9999
        dailylo = 9999
        for lst in self._data:
            hi = lst[self.IDX_DHI]
            lo = lst[self.IDX_DLO]
            if hi is not None and hi > dailyhi: dailyhi = hi
            if lo is not None and lo < dailylo: dailylo = lo
        # Set the daily high and low in the newest sample
        if temperature > dailyhi:
            dailyhi = temperature
        if temperature < dailylo:
            dailylo = temperature
        newest[self.IDX_DHI] = dailyhi
        newest[self.IDX_DLO] = dailylo

        # Insert the newest sample into the front of our window of samples
        self._data.insert(0, newest)


class MovieMaker(object):
    """Makes movies from a bunch of image files."""
    def __init__(self, **kwargs):
        """Constructor method.  expected kwargs are:
            top,
            start_date,
            end_date,
            build_dir
        Other kwargs can include:
            wx_file_path: the full path to the weather data log file (required)
            out_file: the full path where the timelapse movie file gets written.
                (default is 'timelapse.avi')
            do_cleanup: If True, the temporary image files are deleted after the
                movie file is created (default is False)
            charts_only: If True, only the charts are processed (assumes the
                movie images exist in build_dir)
            frames_per_second: The number of frames per second (default is 24)
        """
        default_fps = 18
        self.top = kwargs.get('top', None)
        self.start_date = kwargs.get('start_date', None)
        self.end_date = kwargs.get('end_date', None)
        self.wx_file_path = kwargs.get('wx_file_path', None)
        self.out_file = kwargs.get('out_file', None)
        if not self.out_file:
            self.out_file = (
                'timelapse_%s-%s.avi' % (
                    self.start_date.strftime('%Y%m%d'),
                    self.end_date.strftime('%Y%m%d')
            )
        )
        self.do_cleanup = kwargs.get('do_cleanup', False)
        self.charts_only = kwargs.get('charts_only', False)
        self.frames_per_second = kwargs.get('frames_per_second', default_fps)
        if not self.frames_per_second: self.frames_per_second = default_fps
        if kwargs.has_key('build_dir'):
            self.build_dir = kwargs['build_dir']
        else:
            self.build_dir = tempfile.mkdtemp(
                dir=kwargs.get('build_dir', '/tmp'))
        self.p_idx = 0
        # Filter functions take an instance of this class and return a filtered
        # list of file names.
        self.filter_functions = kwargs.get(
            'filter_functions', [
                filter_between_dates,
            ]
        )
        self.wxwdw = WxDataWindow()

    def _cleanup(self):
        """Cleanup after ourselves."""
        if self.do_cleanup:
            shutil.rmtree(self.build_dir)

    def make_movie(self):
        """Make the movie."""
        self.wx_file_obj = open(self.wx_file_path, 'r')
        for (self.dirpath, self.dirnames, self.filenames) in os.walk(self.top):
            self.dirnames.sort()
            self.filenames.sort()
            self.filter_filenames()
            self.process_files()
            self.filenames = []
        self.wx_file_obj.close()
        output = run(
            'cd "%s" ; ffmpeg -f image2 -r %s -i p%%06d.jpg temp_timelapse.avi'
            % (self.build_dir, self.frames_per_second)
        )
        shutil.move(
            os.path.join(self.build_dir, 'temp_timelapse.avi'),
            self.out_file,
        )
        print("Movie written to: %s" % os.path.abspath(self.out_file))
        # Will only delete the build dir if self.do_cleanup is True
        self._cleanup()

    def filter_filenames(self):
        """Filters the list of filenames, removing names that we do not want
        to process.
        """
        for func in self.filter_functions:
            self.filenames = func(self)

    def process_file(self, fn):
        """Process one image file:
          * Copy it to the build dir
          * look up associated weather data
          * write the weather data to the image file's EXIF header
          * Add weather data graph to the image file
        """
        # Copy it to the build dir
        src = os.path.join(self.dirpath, fn)
        dst = os.path.join(
            self.build_dir,
            'p%06d%s' % (self.p_idx, os.path.splitext(fn)[1])
        )
        chart_pth = os.path.join(
            self.build_dir,
            'p%06d_chart.png' % self.p_idx)

        if os.path.exists(dst) and os.path.exists(chart_pth):
            # Skip processing if the processed file already exists
            return
        try:
            im = Image.open(src)
        except IOError:
            # Problem with this image file; skip it.
            print("Error opening '%s', skipping that file." % src)
            return

        # look up associated weather data
        timestamp = dirpath_to_datetime(src)
        wx_data = get_wx_data(timestamp, self.wx_file_obj)
        temperature = float(wx_data['Temp_outdoor'])
        dewpoint = float(wx_data['Dewpoint'])
        self.wxwdw.put(temperature, dewpoint)

        # Add weather data graph to the image file
        chart = get_wx_chart(timestamp, self.wxwdw)

        try:
            chart.download(file_name=chart_pth)
            chart_image = Image.open(chart_pth)
            # Append the wx data chart to the bottom of the webcam image
            # webcam image is 960x720
            # chart image is 960x110
            im.paste(chart_image, (0, 610, 960, 720))
            im.save(dst)
        except URLError:
            print(
                "Error downloading the sparkline chart for '%s'; skipping"
                % chart_pth)
            return

        # write the weather data to the image file's EXIF header
        if not self.charts_only:
            set_image_data(image_file_path=dst, data=wx_data)

    def process_files(self):
        """Process the image files."""
        print("Filtered files to process in the %s dir:" % self.dirpath)
        for fn in self.filenames:
            self.process_file(fn)
            self.p_idx += 1


class MakeDateAction(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        setattr(
            namespace,
            self.dest,
            datetime.datetime(*time.strptime(values, '%Y-%m-%d')[:5])
        )


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Compiles the webcam photos into a timelapse movie.")
    parser.add_argument(
        'top', default='~/Documents/wx/cam',
        help='Starts in this directory when looking for images.')
    parser.add_argument(
        'start_date', nargs='?', action=MakeDateAction,
        help='Includes photos taken on or after this date')
    parser.add_argument(
        'end_date', nargs='?', action=MakeDateAction,
        help='Includes photos taken on or before this date')
    parser.add_argument(
        'wx_file_path', nargs='?', default=os.path.join(
            os.environ['HOME'], 'Documents/wx/open2300.log'),
        help='Pulls weather data from this file.')
    parser.add_argument(
        '-b', '--build_dir', nargs='?', default='/media/shared_medi',
        help='Staging directory in which image files are copied and edited.')
    parser.add_argument(
        '-o', '--out_file', nargs='?',
        default=None,
        help='Full path to the output movie file.')
    parser.add_argument(
        '-c', '--charts_only', action='store_true', default=False,
        help='Only update the weather charts')
    parser.add_argument(
        '-r', '--frames_per_second', metavar='int', type=int,
        help='Number of frames per second.')

    args = parser.parse_args()

    mm = MovieMaker(**dict(args._get_kwargs()))
    mm.make_movie()
