"""
Utility functions

To run the tests for this module:

python -m doctest -v utils.py
"""


import os
import re
import time
import errno
import pprint
import logging
import datetime
import tempfile
import contextlib
import subprocess


def console_logger(logger_name=None, level=None):
    """A simple debugging logger for sending messages to the console.
    This logger will write messages to the console.
    The default logging level is logging.INFO.

    Call it like this:
    log = console_logger()
    log.debug("my debug message")
    or
    log.error("my error message")
    """
    if logger_name is None:
        logger_name = "console"
    if level is None:
        level = logging.INFO

    # create a debug logger and set level to DEBUG
    logger = logging.getLogger(logger_name)
    logger.setLevel(level)
    ch = logging.StreamHandler()
    ch.setLevel(level)
    ch.setFormatter(logging.Formatter("%(message)s"))
    logger.addHandler(ch)
    return logger



def file_logger(logger_name=None, filename=None, level=None):
    """A simple debugging logger for sending messages to a file.
    This logger will write messages to the filename.  Default logging level is
    logging.INFO.

    Call it like this:
    log = file_logger()
    log.debug("my debug message")
    or
    log.error("my error message")
    """
    if logger_name is None:
        logger_name = "file"
    if filename is None:
        try:
            user = os.environ['USER']
        except KeyError:
            user = 'no_user'
        filename = os.path.join(tempfile.gettempdir(), 'ew.%s.log' % user)
    if level is None:
        level = logging.INFO

    formatter = logging.Formatter(
        "%(asctime)s\t"
        "%(levelname)s\t"
        "%(filename)s:%(lineno)d\t"
        "%(message)s")
    logger = logging.getLogger(logger_name)
    logger.setLevel(level)
    fh = logging.FileHandler(filename)
    fh.setLevel(level)
    fh.setFormatter(
        logging.Formatter(
            "%(asctime)s\t"
            "%(levelname)s\t"
            "%(filename)s:%(lineno)d\t"
            "%(message)s"
        )
    )
    logger.addHandler(fh)
    return logger


log = file_logger(level=logging.WARN)


uptime_re = re.compile(
    '''
    \s* # Any amount of whitespace
    ([0-9]{1,2}:[0-9]{1,2}:[0-9]{1,2}\s*up\s+) # System time, hour:minutes:seconds
    (
        ((?P<a_minutes>\d+)\s+min,) # sometimes only mins are displayed
        | ((?P<b_hours>\d+):(?P<b_minutes>\d+),) # or hours and minutes are
        | ((?P<c_days>\d+)\s+days?,\s+(?P<c_hours>\d+):(?P<c_minutes>\d+),) # or days, hours and minutes are
    )
    ''',
    re.UNICODE | re.VERBOSE
)


def run(cmd, quiet=False):
    """Run a system command and return its output (stdout + stderr) as a
    string.

    >>> from utils import run
    >>> print run('cd /tmp; pwd')
    cd /tmp; pwd ...
    /tmp
    
    >>> print run('cd /tmp; pwd', quiet=True)
    /tmp
    
    >>> output = run('cd /tmp; pwd', quiet=True)
    >>> print output
    /tmp
    
    """
    if not quiet:
        print cmd, "..."
    process = subprocess.Popen(
        cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    process.wait()
    return process.stdout.read() + process.stderr.read()


def run_cmd(cmd, verbose=False):
    """Run a system command and return its output as a dictionary of
      * returncode: The return code of the command
      * stderr: The command output that was sent to stderr
      * stdout: The command output that was sent to stdout

    >>> from utils import run_cmd
    >>> print(run_cmd('cd /tmp; pwd'))
    {'returncode': 0, 'stderr': '', 'stdout': '/tmp'}
    """
    if verbose:
        print cmd, "..."
    try:
        process = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE)
    except OSError, e:
        result = dict(returncode=-1, stdout='', stderr='OSError: ' + e)
    else:
        returncode = process.wait()
        result = dict(returncode=returncode, stdout=process.stdout.read(),
                      stderr=process.stderr.read())
    if verbose:
        pprint.pprint(result)
    return result


@contextlib.contextmanager
def flock(path, wait_delay=.1):
    """A generator that yields a file lock, once it is available.
    Usage is:
    with flock('.lockfile'):
        # do whatever.

    If you want to actually use the file descriptor, 'as' and 'fdopen'
    are all you need:
    with flock('.lockfile') as fd:
        lockfile = os.fdopen(fd, 'r+')

    Thanks Aaron Gallagher
    http://code.activestate.com/recipes/576572/
    """
    while True:
        try:
            fd = os.open(path, os.O_CREAT | os.O_EXCL | os.O_RDWR)
        except OSError, e:
            if e.errno != errno.EEXIST:
                raise
            time.sleep(wait_delay)
            continue
        else:
            break
    try:
        yield fd
    finally:
        os.close(fd)
        os.unlink(path)



class Uptime(object):
    def __init__(self, uptime=None):
        if uptime is None:
            self.uptime = run('uptime', quiet=True).strip()
        else:
            self.uptime = uptime
        self.td = None

        match = uptime_re.search(self.uptime)
        if match:
            d = match.groupdict()
            days = int(d.get('c_days', 0) or 0)
            hours = int(d.get('b_hours', 0) or d.get('c_hours', 0) or 0)
            seconds = int(
                d.get('a_minutes', 0)
                or d.get('b_minutes', 0)
                or d.get('c_minutes', 0)
                or 0) * 60
        log.info(
            "System uptime is: %s days, %s hours, %s seconds"
            % (days, hours, seconds))
        self.td = datetime.timedelta(days=days, hours=hours, seconds=seconds)

    @property
    def timedelta(self):
        """Returns a datetime.timedelta this machine has been running.
        Assumes this is a unix machine that has the 'uptime' command.

        >>> from utils import Uptime
        >>> Uptime(" 12:19:00 up 26 min,  1 user").timedelta.seconds
        1560
        >>> Uptime(" 09:29:12 up 23:29,  3 users,").timedelta.seconds
        84540
        >>> Uptime(" 11:38:57 up 1 day,  1:39,  3 users").timedelta.seconds
        5940
        >>> Uptime(" 11:38:57 up 1 day,  1:39,  3 users").timedelta.days
        1
        >>> Uptime(" 13:40:21 up 116 days,  9:08,  2 users").timedelta.seconds
        32880
        >>> Uptime(" 13:40:21 up 116 days,  9:08,  2 users").timedelta.days
        116
        >>> Uptime(" 12:19:00 up 9:08,  1 user").timedelta.seconds
        32880
        """
        return self.td

    @property
    def seconds(self):
        """Returns the total number of seconds this machine has been up.

        >>> from utils import Uptime
        >>> bool(Uptime().seconds > 1)
        True
        """
        td = self.td
        return (td.days * 86400 + td.seconds)

    def __repr__(self):
        return "<Uptime('%s')>" % self.uptime


def get_uptime(uptime=None):
    """Returns an Uptime instance representing the time this machine has been
    running.
    Assumes this is a unix machine that has the 'uptime' command.

    >>> from utils import get_uptime
    >>> get_uptime(" 12:19:00 up 26 min,  1 user").seconds
    1560
    >>> get_uptime(" 09:29:12 up 23:29,  3 users,").seconds
    84540
    >>> get_uptime(" 11:38:57 up 1 day,  1:39,  3 users").seconds
    92340
    >>> get_uptime(" 13:40:21 up 116 days,  9:08,  2 users").seconds
    10055280
    >>> get_uptime(" 12:19:00 up 9:08,  1 user").seconds
    32880
    """
    return Uptime(uptime)

