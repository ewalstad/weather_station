================================
Weather Station Data Collection
================================

Code for running on the server in the weather station which sends the data
back to the server at home.

Quick Start
===========

pyexiv2 requires dev libraries:

   sudo apt install python-all-dev libboost-python-dev libexiv2-dev scons

If you have trouble installing pyexiv2, try installing exiv2 from source
at the system level, first:

http://dev.exiv2.org/projects/exiv2/wiki/How_do_I_build_Exiv2_on_the_XYZ_platform

Setup the virtual env:

   mkvirtualenv weather_station
   cd $VIRTUAL_ENV
   git clone git@bitbucket.org:ewalstad/weather_station.git
   cd weather_station
   pip install -r requirements.txt
   
Once you have collected some weather station images you can animate them with:

   ./make_movie.py

