#!/bin/bash
#
# Run this script to collect webcam images and weather data.
# Example root cron usage:
# 0,3,6,9,12,15,18,21,24,27,30,33,36,39,42,45,48,51,54,57 * * * * $WX_DIR/collect_data.sh

WX_HOME=/home/ewalstad/wx
SYSTEM_INFO_LOG=$WX_HOME/system_info.log
TMP=/tmp/wxphotos


# Grab an image from the webcam
# See also root's crontab:
#2,5,8,11,14,17,20,23,26,29,32,35,38,41,44,47,50,53,56,59 * * * * rm -rf $TMP && mkdir -p $TMP && ffmpeg -f video4linux2 -s 1280x720 -i /dev/video0 -r 0.3 -t 110 -loglevel quiet "$TMP/wxphoto\%02d.jpg" >/dev/null 2>&1
pth=/home/ewalstad/wx/cam/$(date +%Y)/$(date +%m)/$(date +%d)
tmppth=/tmp/wxphotos
mkdir -p $pth
this_photo=$(ls -1tr /tmp/wxphotos/wxphoto*jpg | tail -n 2 | head -n 1)
# FIXME: Temporary fix to compensate for an upside-down camera:
#convert -rotate 180 $this_photo $this_photo
cp $this_photo $pth/$(date +%H%M).jpg >> $SYSTEM_INFO_LOG
cp $this_photo /home/ewalstad/wx/cam/latest.jpg >> $SYSTEM_INFO_LOG

# Remove web cam photos older than about 3 months
KEEP_DAYS=90
echo "$(date +'%Y-%m-%d %H:%M:%S')	Deleting images older than 90 days: $(find /home/ewalstad/wx/cam -iname '*jpg' -mtime +$KEEP_DAYS -type f -print)" >> $SYSTEM_INFO_LOG
find /home/ewalstad/wx/cam -iname '*jpg' -mtime +$KEEP_DAYS -type f -print | xargs rm -f >> $SYSTEM_INFO_LOG

# Dropbox requires files to be owned by ewalstad
chown -R ewalstad:ewalstad /home/ewalstad/Dropbox/wx

# Hook to run a command as root just once:
if [ ! -f /tmp/wx_run_once ]; then
    touch /tmp/wx_run_once
    crontab -u root /home/ewalstad/virtualenvs/weather_station/weather_station/crontab.root
    crontab -u ewalstad /home/ewalstad/virtualenvs/weather_station/weather_station/crontab.ewalstad
fi
